#drag
一个简单的jQuery拖拽插件。简单，方便，灵活，不需要依赖jQuery UI等组件。

本插件基于jQuery1.8.1.js开发，使用时请保障jQuery版本在此以上。

#优点
不依赖任何css、js，完全自定义拖拽界面；适合有一定经验的人使用。

#说明
没有响亮的名字，没有遵循什么协议；但是，你可以在任何时候，任何地点随便修改，任意使用；
在这个烦躁、堕落、又不尊重尊重知识产权的天朝，一切神马都是浮云。

#auth
auth:sole
email:macore@163.com

#use
```
$('drag-title').drag({
    dir:'xy',
    limit:0,
    down:function(point){
        console.log(point);
    },
    move:function(point){
        console.log(point);
    }
});
```